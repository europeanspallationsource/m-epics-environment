#!/usr/bin/env python2.7

import requests
import json
import getpass

class MyAuth(requests.auth.AuthBase):
    def __init__(self, username):
        self.username = username
        self.password = None

    def __eq__(self, other):
        return all([
            self.username == getattr(other, 'username', None),
            self.password == getattr(other, 'password', None)
        ])

    def __ne__(self, other):
        return not self == other

    def __call__(self, r):
        if not self.password:
            self.password = getpass.getpass("Password of {} for {}: ".format(self.username, r.url.split('?')[0]))
        r.headers['Authorization'] = requests.auth._basic_auth_str(self.username, self.password)
        return r



def get_page(url, **kwargs):
    r = requests.get(url, **kwargs)
    if r.status_code == 200:
        return json.loads(r.text)

    print "Error while retrieving url {url}: {code}".format(url = url, code = r.status_code)
    return None


def get_repos(username = None):
    url = "https://api.bitbucket.org/2.0/repositories/europeanspallationsource?fields=next,size,values.full_name,values.links.clone.*,values.project.type,values.project.key"
    repos = dict()

    if username:
        auth = MyAuth(username)
    else:
        auth = None

    num = 0
    while url is not None:
        rj = get_page(url, auth = auth)

        for r in rj['values']:
            num += 1
            project = r['project']

            if project['type'] != 'project':
                next

            # EEM is EPICS Environment Modules
            if project['key'] == 'EEM':
                ctype = dict()

                for repo in r['links']['clone']:
                    ctype[repo['name']] = repo['href']

                repos[r['full_name']] = ctype
            else:
                pass
        try:
            url = rj['next']
        except KeyError:
            return repos

    return None


if __name__ == "__main__":
    repos = get_repos("k")
    print "Number of repositories: ", len(repos)
    for r in repos:
        print r
